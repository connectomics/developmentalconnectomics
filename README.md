## About Developmental paper code repository:

This repository contains all the code and data used for the following manuscript:   
**Postnatal connectomic development of inhibition in mouse barrel cortex**   
Anjali Gour, Kevin M. Boergens, Natalie Heike,  Yunfeng Hua, Philip Lasterstein, Kun Song and  Moritz Helmstaedter   

* Figure 2e is created by devCon_part6 (figure 16)
* Figure 2b,d are created by devCon_part7 (figure 22)
* Figure 4f is created by devCon_part7 (figure 13)
* Figure 5d is created by devCon_part5 (figures 1, 7, 13, 14)
* Figure 6f is created by devCon_part5 (figure 16)

## License
This project is licensed under the [MIT license](LICENSE).  
Copyright (c) 2020 Department of Connectomics, Max Planck Institute for
Brain Research, D-60438 Frankfurt am Main, Germany
