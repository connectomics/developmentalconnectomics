%% FIGURE_04
% load raw data
clear all
cx_path = 'data/';
[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'));
Age = [7 9 14 28];
%% Axonal synapse density - Soma Axons
ToSel_1 = find(cx_data(:,1)== 5& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_5 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;19 for Soma specificity; 25 for syn density; 33 for binarised innervation
SMSyn_5 = cx_data(ToSel_1,6);
SMDens_5 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
SMBulkInn5 = sum(cx_data(ToSel_1,9)-1)/sum(cx_data(ToSel_1,6)-1);
SMBulkDens5 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

ToSel_1 = find(cx_data(:,1)== 7.1& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_7 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;19 for Soma specificity; 25 for syn density; 33 for binarised innervation
SMSyn_7 = cx_data(ToSel_1,6);
SMDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
SMBulkInn7 = sum(cx_data(ToSel_1,9)-1)/sum(cx_data(ToSel_1,6)-1);
SMBulkDens7 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));

%P9 Soma Axons
%P9L4n1 dataset
ToSel_2 = find(cx_data(:,1)== 9& cx_data(:,3)== 3);
SMInn_9a = (cx_data(ToSel_2,9)-1)./(cx_data(ToSel_2,6)-1); 
SMSyn_9a = cx_data(ToSel_2,6); 
SMDens_9a = (cx_data(ToSel_2,6))./(cx_data(ToSel_2,4));
%P9L4n2 dataset
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3); 
SMInn_9b = (cx_data(ToSel_3,9)-1)./(cx_data(ToSel_3,6)-1);
SMSyn_9b = cx_data(ToSel_3,6); 
SMDens_9b = (cx_data(ToSel_3,6))./(cx_data(ToSel_3,4));
SMBulkInn9 = (sum((cx_data(ToSel_2,9)-1))+sum((cx_data(ToSel_3,9)-1)))/(sum((cx_data(ToSel_2,6)-1))+sum((cx_data(ToSel_3,6)-1)));
SMBulkDens9 = ((sum(cx_data(ToSel_2,6)))+(sum(cx_data(ToSel_3,6))))/((sum(cx_data(ToSel_2,4)))+(sum(cx_data(ToSel_3,4))));


%P14 Soma Axons
%P14L4n1 dataset
ToSel_4 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 3);
SMInn_14a = (cx_data(ToSel_4,9)-1)./(cx_data(ToSel_4,6)-1);
SMSyn_14a = cx_data(ToSel_4,6); 
SMDens_14a = (cx_data(ToSel_4,6))./(cx_data(ToSel_4,4));
%P14L4n2 dataset
ToSel_5 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_14b = (cx_data(ToSel_5,9)-1)./(cx_data(ToSel_5,6)-1);
SMSyn_14b = cx_data(ToSel_5,6); 
SMDens_14b =(cx_data(ToSel_5,6))./(cx_data(ToSel_5,4));
SMBulkInn14 = (sum((cx_data(ToSel_4,9)-1))+sum((cx_data(ToSel_5,9)-1)))/(sum((cx_data(ToSel_4,6)-1))+sum((cx_data(ToSel_5,6)-1)));
SMBulkDens14 = ((sum(cx_data(ToSel_4,6)))+(sum(cx_data(ToSel_5,6))))/((sum(cx_data(ToSel_4,4)))+(sum(cx_data(ToSel_5,4))));

%P28 Soma Axons
ToSel_6 = find(cx_data(:,1)== 28& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_28 =(cx_data(ToSel_6,9)-1)./(cx_data(ToSel_6,6)-1);
SMSyn_28 = cx_data(ToSel_6,6); 
SMDens_28 = (cx_data(ToSel_6,6))./(cx_data(ToSel_6,4));
SMBulkInn28 = sum(cx_data(ToSel_6,9)-1)/sum(cx_data(ToSel_6,6)-1);
SMBulkDens28 = sum(cx_data(ToSel_6,6))/sum(cx_data(ToSel_6,4));

% Combine two P9 and P14 datasets
SMSyn_9 = cat(1,SMSyn_9a,SMSyn_9b); SMInn_9 = cat(1,SMInn_9a,SMInn_9b); SMDens_9 = cat(1,SMDens_9a,SMDens_9b); 
SMSyn_14 = cat(1,SMSyn_14a,SMSyn_14b); SMInn_14 = cat(1,SMInn_14a,SMInn_14b); SMDens_14 = cat(1,SMDens_14a,SMDens_14b); 

% Concatenate all the parameters and sort
SM7all = cat(2,SMSyn_7,SMInn_7,SMDens_7); 
SM9all = cat(2,SMSyn_9,SMInn_9,SMDens_9);
SM14all = cat(2,SMSyn_14,SMInn_14,SMDens_14);
SM28all = cat(2,SMSyn_28,SMInn_28,SMDens_28);
 
SM7sort = sortrows(SM7all,1);
SM9sort = sortrows(SM9all,1);
SM14sort = sortrows(SM14all,1);
SM28sort = sortrows(SM28all,1);
% THRESHOLDING based on nSyn
%thrA = <10; thrB = 10-20; thrC = > 20syn
SelA = find(SM7sort(:,1) < 10);
SelB = find(SM7sort(:,1) > 9 & SM7sort(:,1) <20);
SelC = find(SM7sort(:,1) > 19); 
SM7_thrA_Inn = SM7sort(SelA,2); SM7_thrB_Inn = SM7sort(SelB,2); SM7_thrC_Inn = SM7sort(SelC,2);
SM7_thrA_Dens = SM7sort(SelA,3); SM7_thrB_Dens = SM7sort(SelB,3); SM7_thrC_Dens = SM7sort(SelC,3);

SelA = find(SM9sort(:,1) < 10);
SelB = find(SM9sort(:,1) > 9 & SM9sort(:,1) <20);
SelC = find(SM9sort(:,1) > 19); 
SM9_thrA_Inn = SM9sort(SelA,2); SM9_thrB_Inn = SM9sort(SelB,2); SM9_thrC_Inn = SM9sort(SelC,2);
SM9_thrA_Dens = SM9sort(SelA,3); SM9_thrB_Dens = SM9sort(SelB,3); SM9_thrC_Dens = SM9sort(SelC,3);

SelA = find(SM14sort(:,1) < 10);
SelB = find(SM14sort(:,1) > 9 & SM14sort(:,1) <20);
SelC = find(SM14sort(:,1) > 19); 
SM14_thrA_Inn = SM14sort(SelA,2); SM14_thrB_Inn = SM14sort(SelB,2); SM14_thrC_Inn = SM14sort(SelC,2);
SM14_thrA_Dens = SM14sort(SelA,3); SM14_thrB_Dens = SM14sort(SelB,3); SM14_thrC_Dens = SM14sort(SelC,3);
  
SelA = find(SM28sort(:,1) < 10);
SelB = find(SM28sort(:,1) > 9 & SM28sort(:,1) <20);
SelC = find(SM28sort(:,1) > 19); 
SM28_thrA_Inn = SM28sort(SelA,2); SM28_thrB_Inn = SM28sort(SelB,2); SM28_thrC_Inn = SM28sort(SelC,2);
SM28_thrA_Dens = SM28sort(SelA,3); SM28_thrB_Dens = SM28sort(SelB,3); SM28_thrC_Dens = SM28sort(SelC,3);

% Bootstrapping to get error means
SMInn_7_bstrp = bootstrp(10,@mean,SMInn_7); SE_SMInn_7 = std(SMInn_7_bstrp);
SMInn_9_bstrp = bootstrp(10,@mean,SMInn_9); SE_SMInn_9 = std(SMInn_9_bstrp); 
SMInn_14_bstrp = bootstrp(10,@mean,SMInn_14); SE_SMInn_14 = std(SMInn_14_bstrp); 
SMInn_28_bstrp = bootstrp(10,@mean,SMInn_28); SE_SMInn_28 = std(SMInn_28_bstrp);
BootStrpData_SMInn = cat(2,SMInn_7_bstrp,SMInn_9_bstrp,SMInn_14_bstrp,SMInn_28_bstrp);
 
SMDens_7_bstrp = bootstrp(10,@mean,SMDens_7); SE_SMDens_7 = std(SMDens_7_bstrp);
SMDens_9_bstrp = bootstrp(10,@mean,SMDens_9); SE_SMDens_9 = std(SMDens_9_bstrp); 
SMDens_14_bstrp = bootstrp(10,@mean,SMDens_14); SE_SMDens_14 = std(SMDens_14_bstrp); 
SMDens_28_bstrp = bootstrp(10,@mean,SMDens_28); SE_SMDens_28 = std(SMDens_28_bstrp);
BootStrpData_SMDens = cat(2,SMDens_7_bstrp,SMDens_9_bstrp,SMDens_14_bstrp,SMDens_28_bstrp);
 % Concatenate lines
SE_Dens = cat(1,SE_SMDens_7,SE_SMDens_9,SE_SMDens_14,SE_SMDens_28);
SMBulkDens = cat(1,SMBulkDens7,SMBulkDens9,SMBulkDens14,SMBulkDens28);
SE_Dens_plus = SMBulkDens + SE_Dens;
SE_Dens_minus = SMBulkDens - SE_Dens;
% Concatenate data for box plots
DensData = cat(1,SMDens_7, SMDens_9, SMDens_14, SMDens_28);
% Grouping
g1 = ones(size(SMInn_7));
g2 = 2*ones(size(SMInn_9));
g3 = 3*ones(size(SMInn_14));
g4 = 4*ones(size(SMInn_28));
Group = cat(1,g1,g2,g3,g4);

% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.6])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapse density (syn/um axonal path length)')
xlabel('Postnatal Age (days)')
plot(Age,SMBulkDens,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,SE_Dens_plus,'--k')% lplus err line
plot(Age,SE_Dens_minus,'--k')% minus err line
scatter(ones(numel(SM7_thrA_Dens),1)*7+rand(numel(SM7_thrA_Dens),1)*.75-.45, SM7_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM7_thrB_Dens),1)*7+rand(numel(SM7_thrB_Dens),1)*.75-.45, SM7_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM7_thrC_Dens),1)*7+rand(numel(SM7_thrC_Dens),1)*.75-.45, SM7_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM9_thrA_Dens),1)*9+rand(numel(SM9_thrA_Dens),1)*.75-.45, SM9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM9_thrB_Dens),1)*9+rand(numel(SM9_thrB_Dens),1)*.75-.45, SM9_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM9_thrC_Dens),1)*9+rand(numel(SM9_thrC_Dens),1)*.75-.45, SM9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM14_thrA_Dens),1)*14+rand(numel(SM14_thrA_Dens),1)*.75-.45, SM14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM14_thrB_Dens),1)*14+rand(numel(SM14_thrB_Dens),1)*.75-.45, SM14_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM14_thrC_Dens),1)*14+rand(numel(SM14_thrC_Dens),1)*.75-.45, SM14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(SM28_thrA_Dens),1)*28+rand(numel(SM28_thrA_Dens),1)*.75-.45, SM28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(SM28_thrB_Dens),1)*28+rand(numel(SM28_thrB_Dens),1)*.75-.45, SM28_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(SM28_thrC_Dens),1)*28+rand(numel(SM28_thrC_Dens),1)*.75-.45, SM28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
title('L4 Synapse densities - Soma axons')
SEMerrDens = cat(1,std(SMDens_7)/sqrt(numel(SMDens_7)),std(SMDens_9)/sqrt(numel(SMDens_9)),...
std(SMDens_14)/sqrt(numel(SMDens_14)),std(SMDens_28)/sqrt(numel(SMDens_28)));

%% Axonal synapse density - AD axons
%P7 AD Axons
ToSel_1 = find(cx_data(:,1)== 7.1& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_7 = (cx_data(ToSel_1,7)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;
ADSyn_7 = cx_data(ToSel_1,6);
ADDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
ADBulkInn7 = sum(cx_data(ToSel_1,7)-1)/sum(cx_data(ToSel_1,6)-1);
ADBulkDens7 = sum(cx_data(ToSel_1,6))/sum(cx_data(ToSel_1,4));


%P9 AD Axons
%P9L4n1 dataset
ToSel_2 = find(cx_data(:,1)== 9& cx_data(:,3)== 1);
ADInn_9a = (cx_data(ToSel_2,7)-1)./(cx_data(ToSel_2,6)-1); 
ADSyn_9a = cx_data(ToSel_2,6); 
ADDens_9a = (cx_data(ToSel_2,6))./(cx_data(ToSel_2,4));
%P9L4n2 dataset
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 1); 
ADInn_9b = (cx_data(ToSel_3,7)-1)./(cx_data(ToSel_3,6)-1);
ADSyn_9b = cx_data(ToSel_3,6); 
ADDens_9b = (cx_data(ToSel_3,6))./(cx_data(ToSel_3,4));
ADBulkInn9 = (sum((cx_data(ToSel_2,7)-1))+sum((cx_data(ToSel_3,7)-1)))/(sum((cx_data(ToSel_2,6)-1))+sum((cx_data(ToSel_3,6)-1)));
ADBulkDens9 = ((sum(cx_data(ToSel_2,6)))+(sum(cx_data(ToSel_3,6))))/((sum(cx_data(ToSel_2,4)))+(sum(cx_data(ToSel_3,4))));

%P14 AD Axons
%P14L4n1 dataset
ToSel_4 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 1);
ADInn_14a = (cx_data(ToSel_4,7)-1)./(cx_data(ToSel_4,6)-1);
ADSyn_14a = cx_data(ToSel_4,6); 
ADDens_14a = (cx_data(ToSel_4,6))./(cx_data(ToSel_4,4));
%P14L4n2 dataset
ToSel_5 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_14b = (cx_data(ToSel_5,7)-1)./(cx_data(ToSel_5,6)-1);
ADSyn_14b = cx_data(ToSel_5,6); 
ADDens_14b =(cx_data(ToSel_5,6))./(cx_data(ToSel_5,4));
ADBulkInn14 = (sum((cx_data(ToSel_4,7)-1))+sum((cx_data(ToSel_5,7)-1)))/(sum((cx_data(ToSel_4,6)-1))+sum((cx_data(ToSel_5,6)-1)));
ADBulkDens14 = ((sum(cx_data(ToSel_4,6)))+(sum(cx_data(ToSel_5,6))))/((sum(cx_data(ToSel_4,4)))+(sum(cx_data(ToSel_5,4))));
 
%P28 AD Axons
ToSel_6 = find(cx_data(:,1)== 28& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_28 =(cx_data(ToSel_6,7)-1)./(cx_data(ToSel_6,6)-1);
ADSyn_28 = cx_data(ToSel_6,6); 
ADDens_28 = (cx_data(ToSel_6,6))./(cx_data(ToSel_6,4));
ADBulkInn28 = sum(cx_data(ToSel_6,7)-1)/sum(cx_data(ToSel_6,6)-1);
ADBulkDens28 = sum(cx_data(ToSel_6,6))/sum(cx_data(ToSel_6,4));
 
% Combine two P9 and P14 datasets
ADSyn_9 = cat(1,ADSyn_9a,ADSyn_9b); ADInn_9 = cat(1,ADInn_9a,ADInn_9b); ADDens_9 = cat(1,ADDens_9a,ADDens_9b); 
ADSyn_14 = cat(1,ADSyn_14a,ADSyn_14b); ADInn_14 = cat(1,ADInn_14a,ADInn_14b); ADDens_14 = cat(1,ADDens_14a,ADDens_14b); 
 
% Concatenate all the parameters and sort
AD7all = cat(2,ADSyn_7,ADInn_7,ADDens_7); 
AD9all = cat(2,ADSyn_9,ADInn_9,ADDens_9);
AD14all = cat(2,ADSyn_14,ADInn_14,ADDens_14);
AD28all = cat(2,ADSyn_28,ADInn_28,ADDens_28);
 
AD7sort = sortrows(AD7all,1);
AD9sort = sortrows(AD9all,1);
AD14sort = sortrows(AD14all,1);
AD28sort = sortrows(AD28all,1);
% THRESHOLDING based on nSyn
%thrA = <10; thrB = 10-20; thrC = > 20syn
SelA = find(AD7sort(:,1) < 10);
SelB = find(AD7sort(:,1) > 9 & AD7sort(:,1) <20);
SelC = find(AD7sort(:,1) > 19); 
AD7_thrA_Inn = AD7sort(SelA,2); AD7_thrB_Inn = AD7sort(SelB,2); AD7_thrC_Inn = AD7sort(SelC,2);
AD7_thrA_Dens = AD7sort(SelA,3); AD7_thrB_Dens = AD7sort(SelB,3); AD7_thrC_Dens = AD7sort(SelC,3);
 
SelA = find(AD9sort(:,1) < 10);
SelB = find(AD9sort(:,1) > 9 & AD9sort(:,1) <20);
SelC = find(AD9sort(:,1) > 19); 
AD9_thrA_Inn = AD9sort(SelA,2); AD9_thrB_Inn = AD9sort(SelB,2); AD9_thrC_Inn = AD9sort(SelC,2);
AD9_thrA_Dens = AD9sort(SelA,3); AD9_thrB_Dens = AD9sort(SelB,3); AD9_thrC_Dens = AD9sort(SelC,3);
 
SelA = find(AD14sort(:,1) < 10);
SelB = find(AD14sort(:,1) > 9 & AD14sort(:,1) <20);
SelC = find(AD14sort(:,1) > 19); 
AD14_thrA_Inn = AD14sort(SelA,2); AD14_thrB_Inn = AD14sort(SelB,2); AD14_thrC_Inn = AD14sort(SelC,2);
AD14_thrA_Dens = AD14sort(SelA,3); AD14_thrB_Dens = AD14sort(SelB,3); AD14_thrC_Dens = AD14sort(SelC,3);
  
SelA = find(AD28sort(:,1) < 10);
SelB = find(AD28sort(:,1) > 9 & AD28sort(:,1) <20);
SelC = find(AD28sort(:,1) > 19); 
AD28_thrA_Inn = AD28sort(SelA,2); AD28_thrB_Inn = AD28sort(SelB,2); AD28_thrC_Inn = AD28sort(SelC,2);
AD28_thrA_Dens = AD28sort(SelA,3); AD28_thrB_Dens = AD28sort(SelB,3); AD28_thrC_Dens = AD28sort(SelC,3);
 
% Bootstrapping to get error means
ADInn_7_bstrp = bootstrp(10,@mean,ADInn_7); SE_ADInn_7 = std(ADInn_7_bstrp);
ADInn_9_bstrp = bootstrp(10,@mean,ADInn_9); SE_ADInn_9 = std(ADInn_9_bstrp); 
ADInn_14_bstrp = bootstrp(10,@mean,ADInn_14); SE_ADInn_14 = std(ADInn_14_bstrp); 
ADInn_28_bstrp = bootstrp(10,@mean,ADInn_28); SE_ADInn_28 = std(ADInn_28_bstrp);
BootStrpData_ADInn = cat(2,ADInn_7_bstrp,ADInn_9_bstrp,ADInn_14_bstrp,ADInn_28_bstrp);
 
ADDens_7_bstrp = bootstrp(10,@mean,ADDens_7); SE_ADDens_7 = std(ADDens_7_bstrp);
ADDens_9_bstrp = bootstrp(10,@mean,ADDens_9); SE_ADDens_9 = std(ADDens_9_bstrp); 
ADDens_14_bstrp = bootstrp(10,@mean,ADDens_14); SE_ADDens_14 = std(ADDens_14_bstrp); 
ADDens_28_bstrp = bootstrp(10,@mean,ADDens_28); SE_ADDens_28 = std(ADDens_28_bstrp);
BootStrpData_ADDens = cat(2,ADDens_7_bstrp,ADDens_9_bstrp,ADDens_14_bstrp,ADDens_28_bstrp);
% Concatenate lines
SE_Dens = cat(1,SE_ADDens_7,SE_ADDens_9,SE_ADDens_14,SE_ADDens_28);
ADBulkDens = cat(1,ADBulkDens7,ADBulkDens9,ADBulkDens14,ADBulkDens28);
SE_Dens_plus = ADBulkDens + SE_Dens;
SE_Dens_minus = ADBulkDens - SE_Dens;
SEMerrDens = cat(1,std(ADDens_7)/sqrt(numel(ADDens_7)),std(ADDens_9)/sqrt(numel(ADDens_9)),...
std(ADDens_14)/sqrt(numel(ADDens_14)),std(ADDens_28)/sqrt(numel(ADDens_28)));
% Concatenate data for box plots
DensData = cat(1,ADDens_7, ADDens_9, ADDens_14, ADDens_28);
% Grouping
g1 = ones(size(ADInn_7));
g2 = 2*ones(size(ADInn_9));
g3 = 3*ones(size(ADInn_14));
g4 = 4*ones(size(ADInn_28));
Group = cat(1,g1,g2,g3,g4);
 
% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.6])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapse density (syn/um axonal path length)')
xlabel('Postnatal Age (days)')
plot(Age,ADBulkDens,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,SE_Dens_plus,'--k')% lplus err line
plot(Age,SE_Dens_minus,'--k')% minus err line
scatter(ones(numel(AD7_thrA_Dens),1)*7+rand(numel(AD7_thrA_Dens),1)*.75-.45, AD7_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD7_thrB_Dens),1)*7+rand(numel(AD7_thrB_Dens),1)*.75-.45, AD7_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD7_thrC_Dens),1)*7+rand(numel(AD7_thrC_Dens),1)*.75-.45, AD7_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD9_thrA_Dens),1)*9+rand(numel(AD9_thrA_Dens),1)*.75-.45, AD9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD9_thrB_Dens),1)*9+rand(numel(AD9_thrB_Dens),1)*.75-.45, AD9_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD9_thrC_Dens),1)*9+rand(numel(AD9_thrC_Dens),1)*.75-.45, AD9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD14_thrA_Dens),1)*14+rand(numel(AD14_thrA_Dens),1)*.75-.45, AD14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD14_thrB_Dens),1)*14+rand(numel(AD14_thrB_Dens),1)*.75-.45, AD14_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD14_thrC_Dens),1)*14+rand(numel(AD14_thrC_Dens),1)*.75-.45, AD14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
scatter(ones(numel(AD28_thrA_Dens),1)*28+rand(numel(AD28_thrA_Dens),1)*.75-.45, AD28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
scatter(ones(numel(AD28_thrB_Dens),1)*28+rand(numel(AD28_thrB_Dens),1)*.75-.45, AD28_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
scatter(ones(numel(AD28_thrC_Dens),1)*28+rand(numel(AD28_thrC_Dens),1)*.75-.45, AD28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
title('L4 Synapse densities - AD axons')

%% Proximal and distal dendritic synapses
% Somatic and perisomatic specificity
clear all
[cx_data, cx_data_txt] = xlsread('data/L4_RawData_16-07-2020.xlsx','Sheet8');

ToSel_1 = find(cx_data(:,1)== 7); %use the age and seeding as filter
ProxDens7 = cx_data(ToSel_1,4)./cx_data(ToSel_1,2); 
DistDens7 = (cx_data(ToSel_1,5)- (cx_data(ToSel_1,4)+cx_data(ToSel_1,6)))./cx_data(ToSel_1,2);
BulkProxDens7 = sum(cx_data(ToSel_1,4))/sum(cx_data(ToSel_1,2));
BulkDistDens7 = (sum(cx_data(ToSel_1,5))-(sum(cx_data(ToSel_1,4))+sum(cx_data(ToSel_1,6))))/sum(cx_data(ToSel_1,2));

ToSel_2a = find(cx_data(:,1)== 9.1); %use the age and seeding as filter
ToSel_2b = find(cx_data(:,1)== 9.2);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
ProxDens9 = cx_data(ToSel_2,4)./cx_data(ToSel_2,2); 
DistDens9 = (cx_data(ToSel_2,5)- (cx_data(ToSel_2,4)+cx_data(ToSel_2,6)))./cx_data(ToSel_2,2);
BulkProxDens9 = sum(cx_data(ToSel_2,4))/sum(cx_data(ToSel_2,2));
BulkDistDens9 = (sum(cx_data(ToSel_2,5))-(sum(cx_data(ToSel_2,4))+sum(cx_data(ToSel_2,6))))/sum(cx_data(ToSel_2,2));

ErrProx7 = std(ProxDens7)/sqrt(numel(ProxDens7));
ErrProx9 = std(ProxDens9)/sqrt(numel(ProxDens9));
ErrDist7 = std(DistDens7)/sqrt(numel(DistDens7));
ErrDist9 = std(DistDens9)/sqrt(numel(DistDens9));
ErrProx = cat(1,ErrProx7,ErrProx9);
ErrDist = cat(1,ErrDist7,ErrDist9);

g1=ones(size(ProxDens7)); g2=2*ones(size(ProxDens9));
G = cat(1,g1,g2);
BulkProxData = cat(1,BulkProxDens7,BulkProxDens9);
BulkDistData = cat(1,BulkDistDens7,BulkDistDens9);
ProxData = cat(1,ProxDens7,ProxDens9);
DistData = cat(1,DistDens7,DistDens9);

%Plotting
figure;
boxplot(ProxData,G,'Colors','k')
hold on
boxplot(DistData,G,'Colors','k')
set(gca,'Ylim',[0 0.3])
set(gca,'Xlim',[0.5 2.5])
set(gca,'Xtick',[1 2])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[7 9])
ylabel('Synapse density (syn/um axonal path length)')
xlabel('Postnatal Age (days)')
plot([1 2],BulkProxData,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot([1 2],(BulkProxData+ErrProx),'--r')% lplus err line
plot([1 2],(BulkProxData-ErrProx),'--r')% minus err line
plot([1 2],BulkDistData,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot([1 2],(BulkDistData+ErrDist),'--k')% lplus err line
plot([1 2],(BulkDistData-ErrDist),'--k')% minus err line
scatter(ones(numel(ProxDens7),1)*1+rand(numel(ProxDens7),1)*.25-.125, ProxDens7,'x','MarkerEdgeColor','r','SizeData',150, 'LineWidth',0.1);
scatter(ones(numel(ProxDens9),1)*2+rand(numel(ProxDens9),1)*.25-.125, ProxDens9,'x','MarkerEdgeColor','r','SizeData',150, 'LineWidth',0.1);
scatter(ones(numel(DistDens7),1)*1+rand(numel(DistDens7),1)*.25-.125, DistDens7,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.1);
scatter(ones(numel(DistDens9),1)*2+rand(numel(DistDens9),1)*.25-.125, DistDens9,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.1);
title('L4 Soma Axons - proximal vs distal innervations')
