%% FIGURE 6 - AG
% Synapses onto AIS vs Other targets
clear all
cx_data = xlsread('data/L23_RawData_04-07-2020.xlsx','Sheet1');
%P9 OtherAxons
ToSel_2 = find(cx_data(:,1)== 14& cx_data(:,3)== 2.1);%2.1 for Ax-Ax axons  
P14AIS= cx_data(ToSel_2,8)./ cx_data(ToSel_2,4); 
BulkP14AIS = sum(cx_data(ToSel_2,8))/sum(cx_data(ToSel_2,4));
BulkP14AISinn = sum(cx_data(ToSel_2,8)-1)/sum(cx_data(ToSel_2,6)-1);
P14Oth= (cx_data(ToSel_2,6)- cx_data(ToSel_2,8))./cx_data(ToSel_2,4); 
BulkP14Oth = ((sum(cx_data(ToSel_2,6)))- (sum(cx_data(ToSel_2,8))))/sum(cx_data(ToSel_2,4));
ErrP14AIS = std(P14AIS)/sqrt(numel(P14AIS));
ErrP14Oth = std(P14Oth)/sqrt(numel(P14Oth));

ToSel_3 = find(cx_data(:,1)== 28& cx_data(:,3)== 2.1);%2.1 for Ax-Ax axons  
P28AIS= cx_data(ToSel_3,8)./ cx_data(ToSel_3,4); 
BulkP28AIS = sum(cx_data(ToSel_3,8))/sum(cx_data(ToSel_3,4));
P28Oth= (cx_data(ToSel_3,6)- cx_data(ToSel_3,8))./ cx_data(ToSel_3,4); 
BulkP28AISinn = sum(cx_data(ToSel_3,8)-1)/sum(cx_data(ToSel_3,6)-1);
BulkP28Oth = ((sum(cx_data(ToSel_3,6)))- (sum(cx_data(ToSel_3,8))))/sum(cx_data(ToSel_3,4));
ErrP28AIS = std(P28AIS)/sqrt(numel(P28AIS));
ErrP28Oth = std(P28Oth)/sqrt(numel(P28Oth));

BulkAIS = cat(1,BulkP14AIS,BulkP28AIS);
BulkOth = cat(1,BulkP14Oth,BulkP28Oth);
ErrAIS = cat(1,ErrP14AIS,ErrP28AIS);
ErrOth = cat(1,ErrP14Oth,ErrP28Oth);
g2 =2*ones(size(P14AIS)); g3=3*ones(size(P28AIS));
G=cat(1,g2,g3);
AIS = cat(1,P14AIS,P28AIS);
Oth = cat(1,P14Oth,P28Oth);

figure
hold on
boxplot(AIS,G,'Positions',[1 2],'Colors','k')
boxplot(Oth,G,'Positions',[1 2],'Colors','k')
plot([1 2],BulkAIS,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot([1 2],BulkOth,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot([1 2],BulkAIS+ErrAIS,'--m')
plot([1 2],BulkAIS-ErrAIS,'--m')
plot([1 2],BulkOth+ErrOth,'--','Color',[0.5 0.5 0.5])
plot([1 2],BulkOth-ErrOth,'--','Color',[0.5 0.5 0.5])
scatter(ones(numel(P14AIS),1)*1+rand(numel(P14AIS),1)*.25-.125, P14AIS,'x','MarkerEdgeColor','m','SizeData',150);
scatter(ones(numel(P28AIS),1)*2+rand(numel(P28AIS),1)*.25-.125, P28AIS,'x','MarkerEdgeColor','m','SizeData',150);
scatter(ones(numel(P14Oth),1)*1+rand(numel(P14Oth),1)*.25-.125, P14Oth,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150);
scatter(ones(numel(P28Oth),1)*2+rand(numel(P28Oth),1)*.25-.125, P28Oth,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150);
box off
set(gca,'Ylim',[0 0.15])
set(gca,'Xtick',[1 2])
set(gca,'XtickLabel',[14 28])
set(gca,'TickDir','out')
xlabel('Postnatal ages (days)')
ylabel('Axonal synapse density (AIS syn OR other syn')
title('L2/3 - AIS vs other synapse axonal density in AxAx axons')
