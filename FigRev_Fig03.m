%% FIGURE_03


Age = [7 9 14 28];

%% AVERAGE SYNAPSES PER TARGET - SOMA Axons
clear all
[cx_data, cx_data_txt] = xlsread('data/L4_RawData_16-07-2020.xlsx','Sheet3');
Age = [7 9 14 28];
%P7 Soma Axons
ToSel_1 = find(cx_data(:,1)==7& cx_data(:,3)== 3);
SM_AvgSyn_7 = cx_data(ToSel_1,5)./cx_data(ToSel_1,6);
%P9 Soma Axons
ToSel_2a = find(cx_data(:,1)==9.1& cx_data(:,3)== 3);
ToSel_2b = find(cx_data(:,1)==9.2& cx_data(:,3)== 3);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
SM_AvgSyn_9 = cx_data(ToSel_2,5)./cx_data(ToSel_2,6);
%P14 Soma Axons
ToSel_3a = find(cx_data(:,1)==14.1& cx_data(:,3)== 3);
ToSel_3b = find(cx_data(:,1)==14.2& cx_data(:,3)== 3);
ToSel_3 = cat(1,ToSel_3a,ToSel_3b);
SM_AvgSyn_14 = cx_data(ToSel_3,5)./cx_data(ToSel_3,6);
%P28 Soma Axons
ToSel_4 = find(cx_data(:,1)==28& cx_data(:,3)== 3);
SM_AvgSyn_28 = cx_data(ToSel_4,5)./cx_data(ToSel_4,6);

SM_MeanAvgSyn = cat(1,mean(SM_AvgSyn_7),mean(SM_AvgSyn_9),mean(SM_AvgSyn_14),mean(SM_AvgSyn_28));
Err7 = std(SM_AvgSyn_7)/sqrt(numel(SM_AvgSyn_7));
Err9 = std(SM_AvgSyn_9)/sqrt(numel(SM_AvgSyn_9));
Err14 = std(SM_AvgSyn_14)/sqrt(numel(SM_AvgSyn_14));
Err28 = std(SM_AvgSyn_28)/sqrt(numel(SM_AvgSyn_28));
SM_AvgSyn_Err = cat(1,Err7,Err9,Err14,Err28);

Data_SM_AvgSyn = cat(1,SM_AvgSyn_7,SM_AvgSyn_9,SM_AvgSyn_14,SM_AvgSyn_28);
g1 = ones(size(SM_AvgSyn_7));
g2 = 2*ones(size(SM_AvgSyn_9));
g3 = 3*ones(size(SM_AvgSyn_14));
g4 = 4*ones(size(SM_AvgSyn_28));
Group = cat(1,g1,g2,g3,g4);
%Plotting
figure
boxplot(Data_SM_AvgSyn,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Average number of synapses per soma')
xlabel('Postnatal Age (days)')
plot(Age,SM_MeanAvgSyn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(SM_MeanAvgSyn+SM_AvgSyn_Err),'--k')% lplus err line
plot(Age,(SM_MeanAvgSyn-SM_AvgSyn_Err),'--k')% minus err line
scatter(ones(numel(SM_AvgSyn_7),1)*7+rand(numel(SM_AvgSyn_7),1)*.75-.45, SM_AvgSyn_7,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(SM_AvgSyn_9),1)*9+rand(numel(SM_AvgSyn_9),1)*.75-.45, SM_AvgSyn_9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(SM_AvgSyn_14),1)*14+rand(numel(SM_AvgSyn_14),1)*.75-.45, SM_AvgSyn_14,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(SM_AvgSyn_28),1)*28+rand(numel(SM_AvgSyn_28),1)*.75-.45, SM_AvgSyn_28,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
title('L4 Soma Axons - Average nSyn per target')

% AD axons

%P7 AD Axons
ToSel_1 = find(cx_data(:,1)==7& cx_data(:,3)== 1);
AD_AvgSyn_7 = cx_data(ToSel_1,5)./cx_data(ToSel_1,6);
%P9 Soma Axons
ToSel_2a = find(cx_data(:,1)==9.1& cx_data(:,3)== 1);
ToSel_2b = find(cx_data(:,1)==9.2& cx_data(:,3)== 3);
ToSel_2 = cat(1,ToSel_2a,ToSel_2b);
AD_AvgSyn_9 = cx_data(ToSel_2,5)./cx_data(ToSel_2,6);
%P14 Soma Axons
ToSel_3a = find(cx_data(:,1)==14.1& cx_data(:,3)== 1);
ToSel_3b = find(cx_data(:,1)==14.2& cx_data(:,3)== 1);
ToSel_3 = cat(1,ToSel_3a,ToSel_3b);
AD_AvgSyn_14 = cx_data(ToSel_3,5)./cx_data(ToSel_3,6);
%P28 Soma Axons
ToSel_4 = find(cx_data(:,1)==28& cx_data(:,3)== 1);
AD_AvgSyn_28 = cx_data(ToSel_4,5)./cx_data(ToSel_4,6);
 
AD_MeanAvgSyn = cat(1,mean(AD_AvgSyn_7),mean(AD_AvgSyn_9),mean(AD_AvgSyn_14),mean(AD_AvgSyn_28));
Err7 = std(AD_AvgSyn_7)/sqrt(numel(AD_AvgSyn_7));
Err9 = std(AD_AvgSyn_9)/sqrt(numel(AD_AvgSyn_9));
Err14 = std(AD_AvgSyn_14)/sqrt(numel(AD_AvgSyn_14));
Err28 = std(AD_AvgSyn_28)/sqrt(numel(AD_AvgSyn_28));
AD_AvgSyn_Err = cat(1,Err7,Err9,Err14,Err28);
 
Data_AD_AvgSyn = cat(1,AD_AvgSyn_7,AD_AvgSyn_9,AD_AvgSyn_14,AD_AvgSyn_28);
g1 = ones(size(AD_AvgSyn_7));
g2 = 2*ones(size(AD_AvgSyn_9));
g3 = 3*ones(size(AD_AvgSyn_14));
g4 = 4*ones(size(AD_AvgSyn_28));
Group = cat(1,g1,g2,g3,g4);
%Plotting
figure
boxplot(Data_AD_AvgSyn,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 10])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Average number of synapses per AD')
xlabel('Postnatal Age (days)')
plot(Age,AD_MeanAvgSyn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(AD_MeanAvgSyn+AD_AvgSyn_Err),'--k')% lplus err line
plot(Age,(AD_MeanAvgSyn-AD_AvgSyn_Err),'--k')% minus err line
scatter(ones(numel(AD_AvgSyn_7),1)*7+rand(numel(AD_AvgSyn_7),1)*.75-.45, AD_AvgSyn_7,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(AD_AvgSyn_9),1)*9+rand(numel(AD_AvgSyn_9),1)*.75-.45, AD_AvgSyn_9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(AD_AvgSyn_14),1)*14+rand(numel(AD_AvgSyn_14),1)*.75-.45, AD_AvgSyn_14,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(AD_AvgSyn_28),1)*28+rand(numel(AD_AvgSyn_28),1)*.75-.45, AD_AvgSyn_28,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.0005);
title('L4 AD Axons - Average nSyn per target')

%% MAPPING OF SOMATIC INPUTS AND CORRESPONDING SOMATIC DIAMETERS
clear all
[cx_data, cx_data_txt] = xlsread('data/L4_RawData_16-07-2020.xlsx','Sheet4');
Age = [7 9 14 28];

%Equivalent Soma diameters
ToSel_1 = find(cx_data(:,1)==7); EqDia7 = cx_data(ToSel_1,5);
ToSel_2 = find(cx_data(:,1)==9); EqDia9 = cx_data(ToSel_2,5);
ToSel_3 = find(cx_data(:,1)==14); EqDia14 = cx_data(ToSel_3,5);
ToSel_4 = find(cx_data(:,1)==28); EqDia28 = cx_data(ToSel_4,5);
MeanEqDia = cat(1,mean(EqDia7),mean(EqDia9),mean(EqDia14),mean(EqDia28));
ErrEqDia = cat(1,(std(EqDia7)/sqrt(numel(EqDia7))),(std(EqDia9)/sqrt(numel(EqDia9))),...
                 (std(EqDia14)/sqrt(numel(EqDia14))),(std(EqDia28)/sqrt(numel(EqDia28))));
%All input synapses
ToSel_5 = find(cx_data(:,7)==7); Syn7 = cx_data(ToSel_5,11);
ToSel_6 = find(cx_data(:,7)==9); Syn9 = cx_data(ToSel_6,11);
ToSel_7 = find(cx_data(:,7)==14); Syn14 = cx_data(ToSel_7,11);
ToSel_8 = find(cx_data(:,7)==28); Syn28 = cx_data(ToSel_8,11);
MeanSyn = cat(1,mean(Syn7),mean(Syn9),mean(Syn14),mean(Syn28));
ErrSyn = cat(1,(std(Syn7)/sqrt(numel(Syn7))),(std(Syn9)/sqrt(numel(Syn9))),...
                 (std(Syn14)/sqrt(numel(Syn14))),(std(Syn28)/sqrt(numel(Syn28))));

g1D = ones(size(EqDia7)); g2D = 2*ones(size(EqDia9)); g3D = 3*ones(size(EqDia14));  g4D = 4*ones(size(EqDia28));
g1S = ones(size(Syn7)); g2S = 2*ones(size(Syn9)); g3S = 3*ones(size(Syn14));  g4S = 4*ones(size(Syn28));
Gr_Dia = cat(1,g1D,g2D,g3D,g4D);
Gr_Syn = cat(1,g1S,g2S,g3S,g4S);
EqDia = cat(1,EqDia7,EqDia9,EqDia14,EqDia28);
SynData = cat(1,Syn7,Syn9,Syn14,Syn28);

%Plotting
figure
yyaxis left
hold on
boxplot(SynData,Gr_Syn,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 100])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapses per soma')
xlabel('Postnatal Age (days)')
plot(Age,MeanSyn,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanSyn+ErrSyn),'--m')% lplus err line
plot(Age,(MeanSyn-ErrSyn),'--m')% minus err line
scatter(ones(numel(Syn7),1)*7+rand(numel(Syn7),1)*.75-.45, Syn7,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(Syn9),1)*9+rand(numel(Syn9),1)*.75-.45, Syn9,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(Syn14),1)*14+rand(numel(Syn14),1)*.75-.45, Syn14,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(Syn28),1)*28+rand(numel(Syn28),1)*.75-.45, Syn28,'x','MarkerEdgeColor','m','SizeData',150, 'LineWidth',0.0005);
yyaxis right
boxplot(EqDia,Gr_Dia,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 25])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Somatic diameter (um)')
xlabel('Postnatal Age (days)')
plot(Age,MeanEqDia,'ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanEqDia+ErrEqDia),'--k')% lplus err line
plot(Age,(MeanEqDia-ErrEqDia),'--k')% minus err line
scatter(ones(numel(EqDia7),1)*7+rand(numel(EqDia7),1)*.75-.45, EqDia7,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(EqDia9),1)*9+rand(numel(EqDia9),1)*.75-.45, EqDia9,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(EqDia14),1)*14+rand(numel(EqDia14),1)*.75-.45, EqDia14,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
scatter(ones(numel(EqDia28),1)*28+rand(numel(EqDia28),1)*.75-.45, EqDia28,'x','MarkerEdgeColor','k','SizeData',150, 'LineWidth',0.0005);
title('L4 - Somatic input mapping and diameters')