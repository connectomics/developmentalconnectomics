%% Figure 4b - Inset
% Innervations vs Syn density at P7 and P9 - Soma axons

cx_path = 'data/';
[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020.xlsx'));
Age = [7 9 14 28];

ToSel_1 = find(cx_data(:,1)== 7.1& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_7 = (cx_data(ToSel_1,9)-1)./(cx_data(ToSel_1,6)-1); % 6 for total syn;19 for Soma specificity; 25 for syn density; 33 for binarised innervation
SMSyn_7 = cx_data(ToSel_1,6);
SMDens_7 = (cx_data(ToSel_1,6))./(cx_data(ToSel_1,4));
P7i = SMInn_7;
P7 = SMDens_7;

ToSel_2 = find(cx_data(:,1)== 9& cx_data(:,3)== 3);
SMInn_9a = (cx_data(ToSel_2,9)-1)./(cx_data(ToSel_2,6)-1); 
SMSyn_9a = cx_data(ToSel_2,6); 
SMDens_9a = (cx_data(ToSel_2,6))./(cx_data(ToSel_2,4));
%P9L4n2 dataset
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3); 
SMInn_9b = (cx_data(ToSel_3,9)-1)./(cx_data(ToSel_3,6)-1);
SMSyn_9b = cx_data(ToSel_3,6); 
SMDens_9b = (cx_data(ToSel_3,6))./(cx_data(ToSel_3,4));
P9i = cat(1,SMInn_9a,SMInn_9b);
P9 = cat(1,SMDens_9a,SMDens_9b);
SMInn_9 = P9i;
SMDens_9 = P9;
SMSyn_9 = cat(1,SMSyn_9a,SMSyn_9b);

SM7all = cat(2,SMSyn_7,SMInn_7,SMDens_7); 
SM9all = cat(2,SMSyn_9,SMInn_9,SMDens_9);

SM7sort = sortrows(SM7all,1);
SM9sort = sortrows(SM9all,1);

SelC = find(SM7sort(:,1) > 19); 
SM7_thrC_Inn = SM7sort(SelC,2);
SM7_thrC_Dens = SM7sort(SelC,3);

SelC = find(SM9sort(:,1) > 19);
SM9_thrC_Inn = SM9sort(SelC,2);
SM9_thrC_Dens = SM9sort(SelC,3);

figure;
scatter(SM7_thrC_Dens,SM7_thrC_Inn,'x','MarkerFaceColor','g','MarkerEdgeColor','g','SizeData',50);
hold on
scatter(SM9_thrC_Dens,SM9_thrC_Inn,'x','MarkerFaceColor','b','MarkerEdgeColor','b','SizeData',50);
hold on
daspect([1 1 1])
box off; set(gca,'TickDir','out')
set(gca,'Ylim',[0 0.4])
set(gca,'Xlim',[0 0.4])
set(gca,'Xtick',[0 0.2 0.4])
set(gca,'Ytick',[0 0.2 0.4])
xlabel('Synapse density (syn/um axonal path length)')
ylabel('Innervation fraction')
title('L4 - SomaAxons Innervation fractions vs Density')