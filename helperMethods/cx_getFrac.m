function cx_f = cx_getFrac(cx_on,cx_tot)
    cx_f = sum(cx_on)/sum(cx_tot);
end