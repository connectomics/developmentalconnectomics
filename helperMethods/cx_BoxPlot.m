function cx_BoxPlot(cx_pos,cx_data,cx_width)

    cx_median = median(cx_data);
    cx_lower = prctile(cx_data,25);
    cx_upper = prctile(cx_data,75);
    
    cx_range = cx_pos + [-1 1]*cx_width;
    plot(cx_range,cx_range*0+cx_median,'-k','LineWidth',2);hold on
    plot(cx_range,cx_range*0+cx_lower,'-k','LineWidth',2);hold on
    plot(cx_range,cx_range*0+cx_upper,'-k','LineWidth',2);hold on
    plot([1 1]*cx_range(1),[cx_upper,cx_lower],'-k','LineWidth',2);hold on
    plot([1 1]*cx_range(2),[cx_upper,cx_lower],'-k','LineWidth',2);hold on
    


end