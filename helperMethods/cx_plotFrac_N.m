function cx_plotFrac_N(cx_pos,cx_frac,cx_n,cx_noise)
    % cx_nthreshs: N thresholds from large to small

    xvec = (rand(length(cx_frac),1)-0.5)*cx_noise;
    
    cx_sel = cx_n>=10;
    plot(cx_pos+xvec(cx_sel),cx_frac(cx_sel),'xk','MarkerSize',10);hold on
    cx_sel = cx_n<10;
    plot(cx_pos+xvec(cx_sel),cx_frac(cx_sel),'xk','MarkerSize',4);



end