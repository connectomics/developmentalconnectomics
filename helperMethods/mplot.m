function mp_return = mplot(mp_data,mp_settings)

    if nargout>0
        mp_return = [];
    end
    if (nargin<2)
        mp_settings = '';
    end
    if (size(mp_data,2)==2)
        if (nargout<1)
            plot(mp_data(:,1),mp_data(:,2),mp_settings);
        else
            mp_return = plot(mp_data(:,1),mp_data(:,2),mp_settings);
        end
    end

    if (size(mp_data,2)==3)
        if (nargout<1)
            plot3(mp_data(:,1),mp_data(:,2),mp_data(:,3),mp_settings);
        else
            mp_return = plot3(mp_data(:,1),mp_data(:,2),mp_data(:,3),mp_settings);
        end
    end

    








end