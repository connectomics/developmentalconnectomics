%% AXON GALLERY
%Code for axon gallery
clear all
skel = skeleton('Z:\Data\goura\FigUpdates_Revision\UpdatedAxonGallery\NML\P28_L23_09-07-2020.nml');

treeID = find(cellfun(@(x)any(strfind(x, 'Sm0')==1),skel.names));
treeCount = numel(treeID);
treeName = skel.names(treeID);
%check if that is all the trees you need!
namesCorrect = {};
for i=1:treeCount
    namesCorrect{i} = strrep(treeName{i},'/','Or');
    namesCorrect{i} = strrep(treeName{i},'?','Q');
%     namesCorrect{i} = strrep(names{i},'!','Q');
end
Age = {'P28'};
Layer = {'L2/3'};
Seed = {'Soma'};
namesNew=namesCorrect';
%% Get syn data for plotting
%For Soma axons
AllSyn = skel.getNodesWithComment('syn',treeID,'partial'); %first get all syn
SomaComments = skel.getNodesWithComment('Soma',treeID,'partial');%for soma axons
SomaSeed = skel.getNodesWithComment('seed',treeID,'partial'); 

SomaSyn = {};% for soma axons
SeedSyn = {};
SomaSynwithSeed = {};
for i = 1:treeCount
    SeedSyn{i}= intersect(AllSyn{i},SomaSeed{i});
    SomaSynwithSeed {i}= intersect(AllSyn{i},SomaComments{i});
    SomaSyn{i} = setdiff(SomaSynwithSeed{i},SeedSyn{i});
end
SomaSyn = SomaSyn';
SeedSyn = SeedSyn';
SomaSynwithSeed = SomaSynwithSeed';

OtherSyn = {}; % for soma syn
for i = 1:treeCount
OtherSyn{i}=setdiff(AllSyn{i},SomaSynwithSeed{i});
end
OtherSyn = OtherSyn';

SpecSyn = {};
NonSpecSyn = {};
SeedSynapse = {};
for i = 1:treeCount
SpecSyn{i} = (skel.getNodes(treeID(i),SomaSyn(i)).*skel.scale);
NonSpecSyn{i} = (skel.getNodes(treeID(i),OtherSyn(i)).*skel.scale);
SeedSynapse{i} = (skel.getNodes(treeID(i),SeedSyn(i)).*skel.scale);
end
SpecSyn = SpecSyn';
NonSpecSyn = NonSpecSyn';
SeedSynapse = SeedSynapse';

pathLengths = zeros(treeCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:treeCount
 currentPathLength =(skel.pathLength(treeID(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 pathLengths(i) = currentPathLengthMicrons;
end

TotalSyn = zeros(treeCount,1);
InnFractions = zeros(treeCount,1);
%soma Axons
for i = 1:treeCount
SomaSyn_num = numel(SomaSyn{i});
OtherSyn_num = numel(OtherSyn{i});
TotalSyn(i) = numel(AllSyn{i});
InnFractions(i) = (SomaSyn_num)/((SomaSyn_num) + OtherSyn_num);
end

SynDensity = TotalSyn./pathLengths;
%% For AD axons
AllSyn = skel.getNodesWithComment('syn',treeID,'partial'); %first get all syn
ADComments = skel.getNodesWithComment('AD',treeID,'partial');
ADSeed = skel.getNodesWithComment('seed',treeID,'partial'); 

ADSyn = {};
SeedSyn = {};
ADSynwithSeed = {};
for i = 1:treeCount
    SeedSyn{i}= intersect(AllSyn{i},ADSeed{i});
    ADSynwithSeed {i}= intersect(AllSyn{i},ADComments{i});
    ADSyn{i} = setdiff(ADSynwithSeed{i},SeedSyn{i});
end
ADSyn = ADSyn';
SeedSyn = SeedSyn';
ADSynwithSeed = ADSynwithSeed';

% SeedSyn = {};
% for i = 1:treeCount
% SeedSyn{i}= intersect(AllSyn{i},ADSeed{i});
% end
% SeedSyn = SeedSyn';

OtherSyn = {};
for i = 1:treeCount
OtherSyn{i}=setdiff(AllSyn{i},ADSynwithSeed{i});
end
OtherSyn = OtherSyn';

SpecSyn = {};
NonSpecSyn = {};
SeedSynapse = {};
for i = 1:treeCount
SpecSyn{i} = (skel.getNodes(treeID(i),ADSyn(i)).*skel.scale);
NonSpecSyn{i} = (skel.getNodes(treeID(i),OtherSyn(i)).*skel.scale);
SeedSynapse{i} = (skel.getNodes(treeID(i),SeedSyn(i)).*skel.scale);
end
SpecSyn = SpecSyn';
NonSpecSyn = NonSpecSyn';
SeedSynapse = SeedSynapse';

pathLengths = zeros(treeCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:treeCount
 currentPathLength =(skel.pathLength(treeID(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 pathLengths(i) = currentPathLengthMicrons;
end

TotalSyn = zeros(treeCount,1);
InnFractions = zeros(treeCount,1);

%AD axons
for i = 1:treeCount
ADSyn_num = numel(ADSyn{i});
OtherSyn_num = numel(OtherSyn{i});
TotalSyn(i) = numel(AllSyn{i});
InnFractions(i) = (ADSyn_num)/(ADSyn_num + OtherSyn_num);
end

SynDensity = TotalSyn./pathLengths;
%% For AIS axons
AllSyn = skel.getNodesWithComment('syn',treeID,'partial'); %first get all syn
ISComments = skel.getNodesWithComment('AIS',treeID,'partial'); 
ISSeed = skel.getNodesWithComment('seed',treeID,'partial'); 

ISSyn = {};
ISSynwithSeed = {};
for i = 1:treeCount
    SeedSyn{i}= intersect(AllSyn{i},ISSeed{i});
    ISSynwithSeed {i}= intersect(AllSyn{i},ISComments{i});
    ISSyn{i} = setdiff(ISSynwithSeed{i},SeedSyn{i});
end
ISSyn = ISSyn';
ISSynwithSeed = ISSynwithSeed';

OtherSyn = {};
for i = 1:treeCount
OtherSyn{i}=setdiff(AllSyn{i},ISSynwithSeed{i});
end

OtherSyn = OtherSyn';

SpecSyn = {};
NonSpecSyn = {};
SeedSynapse = {};
for i = 1:treeCount
SpecSyn{i} = (skel.getNodes(treeID(i),ISSyn(i)).*skel.scale)
NonSpecSyn{i} = (skel.getNodes(treeID(i),OtherSyn(i)).*skel.scale)
SeedSynapse{i} = (skel.getNodes(treeID(i),SeedSyn(i)).*skel.scale)
end
SpecSyn = SpecSyn';
NonSpecSyn = NonSpecSyn';
SeedSynapse = SeedSynapse';

pathLengths = zeros(treeCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:treeCount
 currentPathLength =(skel.pathLength(treeID(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 pathLengths(i) = currentPathLengthMicrons;
end

TotalSyn = zeros(treeCount,1);
InnFractions = zeros(treeCount,1);

%AIS axons
for i = 1:treeCount
ISSyn_num = numel(ISSyn{i});
OtherSyn_num = numel(OtherSyn{i});
TotalSyn(i) = numel(AllSyn{i});
InnFractions(i) = (ISSyn_num)/((ISSyn_num) + OtherSyn_num);
end

SynDensity = TotalSyn./pathLengths;
%%
% DoubleSyn = skel.getNodesWithComment('2x',treeID,'partial'); %first get all syn
% IncomingSyn = skel.getNodesWithComment('incoming',treeID,'partial'); %first get all syn
% Syn = [AllSyn DoubleSyn IncomingSyn];

% ISComments = skel.getNodesWithComment('Soma',treeID,'partial');% from the comments choose only cnodes having 'IS' in comments
% ISsyn = intersect(AllSyn,ISComments);%get an intersection to have 'IS"with syn
% OtherSyn = setdiff(AllSyn,ISsyn);
%% Figures for axon galleries
treeColor = [0 0 0] %for black axon skeletons
umScale = true;
lineWidths = 1.5;
SynSize = 36;
SeedSynSize = 50; % seed syn being bigger
Col_NonSyn = [0 0 0]
Col_SomSyn = [1 0 0]
Col_SeedSyn = [1 0 1] % magenta color for seed syn
Bbox = skel.getBbox;
Bbox_X = ((Bbox(1,2) - Bbox(1,1))*11.24)/1000; %bounding box in xc
Bbox_Y = ((Bbox(2,2) - Bbox(2,1))*11.24)/1000;%bounding box in y
Bbox_Z = ((Bbox(3,2) - Bbox(3,1))*30)/1000;%bounding box in z
dirOut = 'Z:\Data\goura\FigUpdates_Revision\UpdatedAxonGallery\P28_L23\SomaAxons\';
for i = 1:treeCount
   NonSyn = (cell2mat(NonSpecSyn(i)))./1000; 
   SomSyn = (cell2mat(SpecSyn(i)))./1000; 
   SeedSy = (cell2mat(SeedSynapse(i)))./1000;
   figure ('Position',get(0,'Screensize')) %saves figure in full size window
   skel.plot(treeID(i),treeColor,umScale,lineWidths)
   hold on
   scatter3(NonSyn(:,1),NonSyn(:,2),NonSyn(:,3),SynSize,Col_NonSyn,'filled')
   hold on
   scatter3(SomSyn(:,1),SomSyn(:,2),SomSyn(:,3),SynSize,Col_SomSyn,'filled')
   hold on
   scatter3(SeedSy(:,1),SeedSy(:,2),SeedSy(:,3),SeedSynSize,Col_SeedSyn,'filled')
   hold on
   daspect([1 1 1])
   set(gca,'Xlim',[0 100]); % for P5 L4 - 0 90; for P7 L4, P9L4n1, P14L4n2, P28L4 - [0 80], P9L4n2 [0 120], P9L23 - [0 100]
   set(gca,'Ylim',[0 100]);%for P5L4 - 0 120 P7 L4, P9L4n1, p14L4n2, P28L4 - [0 110], P9L4n2 [0 90], P9L23 - [0 120], P!$L2 - [0 90]
   set(gca,'zlim',[0 250]);%for p5 L4 - 0 80; P7 L4, P9L4n1, P28L4 - [0 180], P9L4n2 [0 180]
   set(gca,'TickDir','out') 
   yticks([0 50 100 150]);
   xticks([0 50 100 150]);
%    yticklabels({'0�m','50�m','100�m','150�m'});% for cortically uproght datasets
   yticklabels({'100�m','50�m','0�m'}); % for P7L4 and P28L4/23
   xticklabels({'0�m','50�m','100�m'});
%    xticklabels({'150�m','100�m','50�m','0�m'});% for P9L4n2
%    set(gca,'YAxisLocation','right'); %for P28
   set(gca,'YAxisLocation','left'); %
   set(gca,'XAxisLocation','bottom'); %for  P14L2
%       set(gca,'XAxisLocation','top'); %for  P9L4n2
%    set(gca,'Ydir','reverse');% for dataset with pia at bottom (p7L4n1)
   box off 
   dimPia=[0.03 .6 .3 .3];
   dimWM=[0.03 0 .3 .12];
   dimStrPia = 'Pia ?';
   dimStrWM = 'WM ?'
   annotation('textbox',dimPia,'String',dimStrPia,'FitBoxToText','on','EdgeColor','none','FontSize',14);
   annotation('textbox',dimWM,'String',dimStrWM,'FitBoxToText','on','EdgeColor','none','FontSize',14);
   set(0,'DefaultTextInterpreter','none');
%    title(treeName(i))
   str = sprintf('Age : %s \nLayer : %s \nSeed : %s \nAxonID : %s \nSynapse density : %.3f syn/�m \nInnervation fraction (excluding seed synapse) : %.1f%% \nPath length : %.1f�m \nTotal synapses : %d',Age{1}, Layer{1}, Seed{1}, treeName{i},SynDensity(i),(InnFractions(i)*100),pathLengths(i),TotalSyn(i));
   title(str)
   view([0 90])
%    annotation('textbox',[0.7 0.825 0.1 0.1],'String',str) % position of textbox for P7 L4 - [0.7 0.825 0.1 0.1]
%    view ({0 -90})% for xy view for P5L4; p7l4n2
%    view([0 90])%for xy view  for P7 L4
%    view([0 90])% for xy view for P9 L4 n1; P14 L4 n2; for P9L4n2 - [0 -90] 
%    camroll(90)% for xy vie - pia top, WM bottom (P9L4n2) dataset - (90); P28L4 - (-90); P14L2 - (90)

   print(fullfile(fullfile(dirOut,namesNew{i})),'-dpdf','-fillpage')
end
sprintf('**** done ****')
