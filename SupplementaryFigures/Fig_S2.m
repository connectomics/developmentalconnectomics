%% SUPPLEMENTARY FIGURE 2 - 
%Gour et al DevConn
%% Fig.S2B Target proximity analysis
[cx_data, cx_data_txt] = xlsread('L4_RawData_16-07-2020_corr_21-01-2021.xlsx','Sheet5');
P7prox = cx_data((1:21),(1:3));
P7Inn = cx_data((1:21),10);
P9aprox = cx_data((24:53),(1:3));
P9aInn= cx_data((24:53),10);
P9bprox = cx_data((56:77),(1:3));
P9bInn = cx_data((56:77),10);
P9prox = cat(1,P9aprox,P9bprox);
P9Inn = cat(1,P9aInn,P9bInn);


figure
%for 1um
subplot(1,3,1)
hold on
scatter(P7prox(:,1),P7Inn,'xg','SizeData',70)
scatter(P9prox(:,1),P9Inn,'xy','SizeData',70)
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 1])
title('radius thr = 1um')
xlabel('fractional axon length in proximity')
ylabel('Soma preference')
daspect([1 1 1])

%for 3um
subplot(1,3,2)
hold on
scatter(P7prox(:,2),P7Inn,'xg','SizeData',70)
scatter(P9prox(:,2),P9Inn,'xy','SizeData',70)
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 1])
title('radius thr = 3um')
xlabel('fractional axon length in proximity')
ylabel('Soma preference')
daspect([1 1 1])

%for 5um
subplot(1,3,3)
hold on
scatter(P7prox(:,3),P7Inn,'xg','SizeData',70)
scatter(P9prox(:,3),P9Inn,'xy','SizeData',70)
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 1])
title('radius thr = 5um')
xlabel('fractional axon length in proximity')
ylabel('Soma preference')
daspect([1 1 1])
%% FigS2C - volumetric synapse density
clear all
[cx_data, cx_data_txt] = xlsread('L4_RawData_16-07-2020_corr_21-01-2021.xlsx','Sheet6');
P7 = cx_data((1:2),13);
P9 = cx_data((3:7),13);
Mean = [mean(P7) mean(P9)];
g1 = ones(size(P9));

figure
boxplot(P9,g1,'Positions',2,'Colors','k')
hold on
plot([1 2],Mean,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
scatter(ones(numel(P7),1)*1+rand(numel(P7),1)*.25-.125, P7,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.005);
scatter(ones(numel(P9),1)*2+rand(numel(P9),1)*.25-.125, P9,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',150, 'LineWidth',0.005);
box off
set(gca,'Ylim',[0 0.3])
set(gca,'Xlim',[0 3])
set(gca,'Xtick',[0 1 2])
set(gca,'Xticklabel',{'','7','9'})
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Volumetric synapse density')
xlabel('Postnatal Age (days)')

%% FigS2D - Hstogram of soma innervation perference of soma-seeded axons at P7 P9
%% preference distribution at P9 for somata (bimodal?), also compare with P7

 

cx_path = '.'

 

[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020_corr_21-01-2021.xlsx'));

cx_rawdataL4_Soma = cx_data(160:224,7:13); % all P9

cx_rawdataL4_Soma = cx_data(160:211,7:13); % initial P9

 

cx_bulk = (sum(cx_rawdataL4_Soma(:,3))-size(cx_rawdataL4_Soma,1))/(sum(sum(cx_rawdataL4_Soma))-size(cx_rawdataL4_Soma,1));

sprintf('P9 soma all axons: %.2f%%, n=%d',cx_bulk*100,size(cx_rawdataL4_Soma,1))

 

cx_fractions = (cx_rawdataL4_Soma(:,3)-1)./(sum(cx_rawdataL4_Soma,2)-1);

figure

subplot(1,2,2)

hist(cx_fractions,[0:0.05:1]);

title('somatic preference distribution P9')

set(gca,'XLim',[-0.05 1.05],'YLim',[0 20]);box off;set(gca,'TickDir','out');

xlabel('p(soma|soma) per axon');

ylabel('# axons');

set(gca,'XTick',[0:0.1:1]);

 

cx_rawdataL4_Soma = cx_data(48:82,7:13);    % all P7

cx_rawdataL4_Soma = cx_data(48:68,7:13);    % initial P7

cx_bulk = (sum(cx_rawdataL4_Soma(:,3))-size(cx_rawdataL4_Soma,1))/(sum(sum(cx_rawdataL4_Soma))-size(cx_rawdataL4_Soma,1));

sprintf('P7 soma all axons: %.2f%%, n=%d',cx_bulk*100,size(cx_rawdataL4_Soma,1))

cx_fractions = (cx_rawdataL4_Soma(:,3)-1)./(sum(cx_rawdataL4_Soma,2)-1);

subplot(1,2,1)

hist(cx_fractions,[0:0.05:1]);

title('somatic preference distribution P7')

set(gca,'XLim',[-0.05 1.05],'YLim',[0 20]);box off;set(gca,'TickDir','out');

xlabel('p(soma|soma) per axon');

ylabel('# axons');

set(gca,'XTick',[0:0.1:1]);

 

% now again including split-branch data

 



[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_16-07-2020_corr_21-01-2021.xlsx'),10);

 

cx_branches = cx_data(1:573,:);

cx_densities = cx_branches(:,5)./cx_branches(:,4);

figure

aa=histc(cx_densities,[0:0.05:10]);

bar([0:0.05:10],aa/sum(aa));hold on

 

 

cx_branches = cx_data(575:1210,:);

cx_densities = cx_branches(:,5)./cx_branches(:,4);

aa=hist(cx_densities,[0:0.05:10],'r');hold on

bar([0:0.05:10],aa/sum(aa),'r');hold on

 

 

cx_preferences = cx_branches(:,6)./cx_branches(:,5);

figure

hist(cx_preferences,[0:0.05:1]);