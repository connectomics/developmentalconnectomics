%% Excitatory controls analysis for Suppl. Fig. 5

 

cx_path = '.'

 

[cx_data, cx_data_txt] = xlsread(fullfile(cx_path,'L4_RawData_07-07-2020.xlsx'),9); % data in sheet 9

 

cx_targetlabels = cx_data_txt(2,6:16);

 

cx_controlResults = {};

cx_controlDensities = {};

 

% P7============================================

cx_data_thisSel = cx_data(1:20,6:16);

cx_data_thisLengths = cx_data(1:20,4);

cx_data_thisSel_aggreg = cx_data_thisSel(:,[8 6 3 9 10]);

cx_targetLabels_aggr = {'Soma','Shaft','Spine','AIS','Glia'};

cx_data_thisSel_aggreg(:,1) = sum(cx_data_thisSel(:,[8 11]),2);

cx_data_thisSel_aggreg(:,2) = sum(cx_data_thisSel(:,[1 6 7]),2);

cx_data_thisSel_aggreg(:,3) = sum(cx_data_thisSel(:,[2 3 4 5]),2);

 

 

figure

for i=1:size(cx_data_thisSel_aggreg,1)

    plot(cx_data_thisSel_aggreg(i,:)/sum(cx_data_thisSel_aggreg(i,:)),'x-','Color',[0.8 0.8 0.8]);hold on

end

set(gca,'XTick',[1:5],'XTickLabel',cx_targetLabels_aggr);

title('P7, AD innervation');

 

cx_data_ExcControlP7 = cx_data(71:74,5:15);

cx_data_ExcControlP7_aggr = cx_data_ExcControlP7(:,[8 6 3 9 10]);

cx_data_ExcControlP7_aggr(:,1) = sum(cx_data_ExcControlP7(:,[8 11]),2);

cx_data_ExcControlP7_aggr(:,2) = sum(cx_data_ExcControlP7(:,[1 6 7]),2);

cx_data_ExcControlP7_aggr(:,3) = sum(cx_data_ExcControlP7(:,[2 3 4 5]),2);

 

 

for i=1:size(cx_data_ExcControlP7_aggr,1)

    plot(cx_data_ExcControlP7_aggr(i,:)/sum(cx_data_ExcControlP7_aggr(i,:)),'x-','Color','m');hold on

end

 

cx_all = 1:size(cx_data_thisSel_aggreg,1);

cx_data_thisSel_aggreg_norm = cx_data_thisSel_aggreg./repmat(sum(cx_data_thisSel_aggreg,2),[1 size(cx_data_thisSel_aggreg,2)]);

cx_sel={};

cx_sel{1} = cx_all;

cx_sel{2} = find(cx_data_thisSel_aggreg_norm(:,3)<0.2)

cx_sel{3} = find((cx_data_thisSel_aggreg_norm(:,3)<0.2)+cx_data_thisSel_aggreg_norm(:,1)>0.1)

cx_sel{4} = find((cx_data_thisSel_aggreg_norm(:,3)<0.1)+cx_data_thisSel_aggreg_norm(:,1)>0.1)

cx_sel{5} = find((cx_data_thisSel_aggreg_norm(:,3)<0.1))

cx_sel{6} = find((cx_data_thisSel_aggreg_norm(:,3)<0.05))

cx_thisBulk=[];cx_thisDens=[];

for i=1:size(cx_sel,2)

    cx_thisBulk_temp = sum(cx_data_thisSel(cx_sel{i},[1 2 4]),2)-1;

    cx_thisBulk(i) = sum(cx_thisBulk_temp)/(sum(sum(cx_data_thisSel(cx_sel{i},:)))-length(cx_sel{i}))

    text(3,1-0.1*i,sprintf('sel %d: %.2f%%',i, cx_thisBulk(i)*100));text(4,1-0.1*i,sprintf('%d excl of %d',length(setdiff(cx_sel{1},cx_sel{i})),length(cx_all)));

    cx_thisDens(i) = sum(sum(cx_data_thisSel(cx_sel{i},:),2))/sum(cx_data_thisLengths(cx_sel{i}));

end

 

set(gca,'TickDir','out');box off

 

cx_controlResults{1} = cx_thisBulk;

cx_controlDensities{1} = cx_thisDens;

 

% P9============================================

cx_data_thisSel = cx_data(23:42,6:16);

cx_data_thisLengths = cx_data(23:42,4);

cx_data_thisSel_aggreg = cx_data_thisSel(:,[8 6 3 9 10]);

cx_targetLabels_aggr = {'Soma','Shaft','Spine','AIS','Glia'};

cx_data_thisSel_aggreg(:,1) = sum(cx_data_thisSel(:,[8 11]),2);

cx_data_thisSel_aggreg(:,2) = sum(cx_data_thisSel(:,[1 6 7]),2);

cx_data_thisSel_aggreg(:,3) = sum(cx_data_thisSel(:,[2 3 4 5]),2);

 

 

figure

for i=1:size(cx_data_thisSel_aggreg,1)

    plot(cx_data_thisSel_aggreg(i,:)/sum(cx_data_thisSel_aggreg(i,:)),'x-','Color',[0.8 0.8 0.8]);hold on

end

set(gca,'XTick',[1:5],'XTickLabel',cx_targetLabels_aggr);

title('P9, AD innervation');

 

cx_data_ExcControlP7 = cx_data(76:78,5:15);

cx_data_ExcControlP7_aggr = cx_data_ExcControlP7(:,[8 6 3 9 10]);

cx_data_ExcControlP7_aggr(:,1) = sum(cx_data_ExcControlP7(:,[8 11]),2);

cx_data_ExcControlP7_aggr(:,2) = sum(cx_data_ExcControlP7(:,[1 6 7]),2);

cx_data_ExcControlP7_aggr(:,3) = sum(cx_data_ExcControlP7(:,[2 3 4 5]),2);

 

 

for i=1:size(cx_data_ExcControlP7_aggr,1)

    plot(cx_data_ExcControlP7_aggr(i,:)/sum(cx_data_ExcControlP7_aggr(i,:)),'x-','Color','m');hold on

end

 

cx_all = 1:size(cx_data_thisSel_aggreg,1);

cx_data_thisSel_aggreg_norm = cx_data_thisSel_aggreg./repmat(sum(cx_data_thisSel_aggreg,2),[1 size(cx_data_thisSel_aggreg,2)]);

cx_sel={};

cx_sel{1} = cx_all;

cx_sel{2} = find(cx_data_thisSel_aggreg_norm(:,2)>0.7)

cx_sel{3} = find((cx_data_thisSel_aggreg_norm(:,2)>0.7)+cx_data_thisSel_aggreg_norm(:,1)>0.1)

cx_sel{4} = find((cx_data_thisSel_aggreg_norm(:,3)<0.3)+cx_data_thisSel_aggreg_norm(:,1)>0.1)

cx_thisBulk=[];cx_thisDens=[];

for i=1:size(cx_sel,2)

    cx_thisBulk_temp = sum(cx_data_thisSel(cx_sel{i},[1 2 4]),2)-1;

    cx_thisBulk(i) = sum(cx_thisBulk_temp)/(sum(sum(cx_data_thisSel(cx_sel{i},:)))-length(cx_sel{i}))

    text(3,1-0.1*i,sprintf('sel %d: %.2f%%',i, cx_thisBulk(i)*100));text(4,1-0.1*i,sprintf('%d excl of %d',length(setdiff(cx_sel{1},cx_sel{i})),length(cx_all)));

    cx_thisDens(i) = sum(sum(cx_data_thisSel(cx_sel{i},:),2))/sum(cx_data_thisLengths(cx_sel{i}));

end

set(gca,'TickDir','out');box off

cx_controlResults{2} = cx_thisBulk;

cx_controlDensities{2} = cx_thisDens;

 

% P14============================================

cx_data_thisSel = cx_data(44:67,6:16);

cx_data_thisLengths = cx_data(44:67,4);

cx_data_thisSel_aggreg = cx_data_thisSel(:,[8 6 3 9 10]);

cx_targetLabels_aggr = {'Soma','Shaft','Spine','AIS','Glia'};

cx_data_thisSel_aggreg(:,1) = sum(cx_data_thisSel(:,[8 11]),2);

cx_data_thisSel_aggreg(:,2) = sum(cx_data_thisSel(:,[1 6 7]),2);

cx_data_thisSel_aggreg(:,3) = sum(cx_data_thisSel(:,[2 3 4 5]),2);

 

 

figure

for i=1:size(cx_data_thisSel_aggreg,1)

    plot(cx_data_thisSel_aggreg(i,:)/sum(cx_data_thisSel_aggreg(i,:)),'x-','Color',[0.8 0.8 0.8]);hold on

end

set(gca,'XTick',[1:5],'XTickLabel',cx_targetLabels_aggr);

title('P14, AD innervation');

 

cx_data_ExcControlP7 = cx_data(80:82,5:15);

cx_data_ExcControlP7_aggr = cx_data_ExcControlP7(:,[8 6 3 9 10]);

cx_data_ExcControlP7_aggr(:,1) = sum(cx_data_ExcControlP7(:,[8 11]),2);

cx_data_ExcControlP7_aggr(:,2) = sum(cx_data_ExcControlP7(:,[1 6 7]),2);

cx_data_ExcControlP7_aggr(:,3) = sum(cx_data_ExcControlP7(:,[2 3 4 5]),2);

 

 

for i=1:size(cx_data_ExcControlP7_aggr,1)

    plot(cx_data_ExcControlP7_aggr(i,:)/sum(cx_data_ExcControlP7_aggr(i,:)),'x-','Color','m');hold on

end

 

cx_all = 1:size(cx_data_thisSel_aggreg,1);

cx_data_thisSel_aggreg_norm = cx_data_thisSel_aggreg./repmat(sum(cx_data_thisSel_aggreg,2),[1 size(cx_data_thisSel_aggreg,2)]);

cx_sel={};

cx_sel{1} = cx_all;

cx_sel{2} = find(cx_data_thisSel_aggreg_norm(:,2)>0.5)

cx_sel{3} = find((cx_data_thisSel_aggreg_norm(:,3)<0.3)+cx_data_thisSel_aggreg_norm(:,1)>0.1)

cx_thisBulk=[];cx_thisDens=[];

for i=1:size(cx_sel,2)

    cx_thisBulk_temp = sum(cx_data_thisSel(cx_sel{i},[1 2 4]),2)-1;

    cx_thisBulk(i) = sum(cx_thisBulk_temp)/(sum(sum(cx_data_thisSel(cx_sel{i},:)))-length(cx_sel{i}))

    text(3,1-0.1*i,sprintf('sel %d: %.2f%%',i, cx_thisBulk(i)*100));text(4,1-0.1*i,sprintf('%d excl of %d',length(setdiff(cx_sel{1},cx_sel{i})),length(cx_all)));

    cx_thisDens(i) = sum(sum(cx_data_thisSel(cx_sel{i},:),2))/sum(cx_data_thisLengths(cx_sel{i}));

end

 

cx_controlResults{3} = cx_thisBulk;

cx_controlDensities{3} = cx_thisDens;

set(gca,'TickDir','out');box off

 

figure

subplot(1,2,1)

plot(7,cx_controlResults{1},'x');hold on;plot(7,cx_controlResults{1}(1),'o');

plot(9,cx_controlResults{2},'x');plot(9,cx_controlResults{2}(1),'o');

plot(14,cx_controlResults{3},'x');plot(14,cx_controlResults{3}(1),'o');

set(gca,'XLim',[0 20],'YLim',[0 0.5]);

xlabel('postnatal age (days)');ylabel('p(AD|AD)');

set(gca,'TickDir','out');box off

subplot(1,2,2)

plot(7,cx_controlDensities{1},'x');hold on;plot(7,cx_controlDensities{1}(1),'o');

plot(9,cx_controlDensities{2},'x');plot(9,cx_controlDensities{2}(1),'o');

plot(14,cx_controlDensities{3},'x');plot(14,cx_controlDensities{3}(1),'o');

set(gca,'XLim',[0 20],'YLim',[0 0.2]);

xlabel('postnatal age (days)');ylabel('synapse density (per �m)');

 

 

set(gca,'TickDir','out');box off