function hash = getGitHash()
    % hash = getGitHash()
    %   Get hash of current git commit.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>

    % run git
    [status, hash] = ...
        system('git rev-parse HEAD');
    
    % check exit code
    assert(not(status));
    
    % remove excess white spaces
    hash = strtrim(hash);
end