function bboxMat = convertWebknossosToMatlabBbox(bboxWeb)
    % Convert bounding box used in webKnossos to the one used in e.g. readKnossoRoi, pipeline repo etc.

    bboxMat = reshape(bboxWeb, 3, 2);
    bboxMat(:,1) = bboxMat(:,1) + 1;
    bboxMat(:,2) = bboxMat(:,1) + bboxMat(:,2) - 1;

end

